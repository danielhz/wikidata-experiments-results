# Wikidata experiment results #

This repositoru contains results for Wikidata experiments.

## Used engines ##

### Virtuoso ###

Virtuoso Open Source (Version 7.2.3-dev.3215-pthreads as of Jan 12 2016)

### Blazegraph ###

Blazegraph 2.1.0
(blazegraph.jar file from https://sourceforge.net/projects/bigdata/files/bigdata/2.1.0/blazegraph.jar/download).
Java SE (ORACLE) version "1.7.0_80".
Java Runtime Environment build 1.7.0_80-b15.
Java HotSpot 64-Bit Server VM (build 24.80-b11, mixed mode).